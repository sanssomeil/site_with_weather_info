from django.urls import path

from . import views

app_name = 'weather'
urlpatterns = [
    path('', views.index, name='index'),
    path('get-default/', views.get_info_about_default_location, name='get_default'),
    path('show-default/', views.show_info_about_default_location, name='show_default'),
    path('search-weather-results/', views.search_location_for_get_weather, name='search_results_for_get_weather'),
    path('search-results/', views.search_location_for_change_settings, name='search_results_for_change_settings'),
    path('get-another/', views.get_info_about_another_location, name='get_another'),
    path('show-all/', views.show_all_saved_info_about_locations, name='show_all'),
    path('show-favorite-locations/', views.show_info_about_favorite_locations, name='show_favorite'),
    path('settings/', views.go_to_settings, name='settings'),
    path('change-api-key/', views.change_api_key_in_settings, name='change_api_key'),
    path('change-default-location/', views.change_default_location_in_settings, name='change_default_location'),
    path('change-cron-settings/', views.change_cron_settings, name='change_cron_settings'),
    path('delete-from-cron/', views.delete_favorite_location_from_cron_settings, name='delete_from_cron'),
    path('search-favorite-location/', views.search_location_for_add_favorite_in_cron_settings, name='search_results_for_add_favorite'),
    path('add-favorite-location/', views.add_favorite_location_in_cron_settings, name='add_favorite_location'),
]

# Generated by Django 4.0.2 on 2022-03-26 20:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('weather', '0003_favoritelocation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='favoritelocation',
            name='favorite_location_key',
            field=models.CharField(max_length=8),
        ),
    ]

import datetime

from django.utils.datastructures import MultiValueDictKeyError
from django.shortcuts import render

from weather.check_new_api_key import check_new_api_key_on_errors
from weather.models import Location, Settings, Weather, FavoriteLocation
from weather.search_location import search_location_by_name
from weather.send_request_for_weather import send_request_about_weather_in_location


def index(request):
    try:
        setting = Settings.objects.get()  # получение настроек приложения из БД
        if setting:
            default_location_key = setting.default_location_key_id
            location = Location.objects.filter(
                location_key__exact=default_location_key)  # получение данных из БД о названии локации по умолчанию
            default_location_name = str(location[0])
            return render(request, 'weather/index.html', {'default_location_name': default_location_name})
    except:
        return render(request, 'weather/index.html', {'default_location_name': 'не определена'})


def get_info_about_default_location(request):
    setting = Settings.objects.get()  # получение настроек приложения из БД
    default_location_key = setting.default_location_key_id
    location = Location.objects.filter(
        location_key__exact=default_location_key)  # получение данных из БД о названии локации по умолчанию
    default_location_name = str(location[0])
    # запрос информации о погоде с сайта accuweather.com
    url_forecast = 'http://dataservice.accuweather.com/forecasts/v1/daily/1day/'
    url_current_conditions = 'http://dataservice.accuweather.com/currentconditions/v1/'
    answer_for = send_request_about_weather_in_location(url_forecast, default_location_key)
    answer_cc = send_request_about_weather_in_location(url_current_conditions, default_location_key)
    d1 = datetime.datetime.strptime(answer_for['DailyForecasts'][0]['Date'],
                                    "%Y-%m-%dT%H:%M:%S%z")  # переводим дату в удобный формат
    date_of = d1.strftime('%d.%m.%Y')
    t_min = str(answer_for['DailyForecasts'][0]['Temperature']['Minimum']['Value']) + ' °' + \
            answer_for['DailyForecasts'][0]['Temperature']['Minimum']['Unit']
    t_max = str(answer_for['DailyForecasts'][0]['Temperature']['Maximum']['Value']) + ' °' + \
            answer_for['DailyForecasts'][0]['Temperature']['Maximum']['Unit']
    pressure = str(round(answer_cc[0]['Pressure']['Metric']['Value'] * 0.75006375541921)) + ' мм рт.ст. ' + \
               answer_cc[0]['PressureTendency']['LocalizedText']
    description = 'Днём: ' + answer_for['DailyForecasts'][0]['Day']['LongPhrase'] + '. Ночью: ' + \
                  answer_for['DailyForecasts'][0]['Night']['LongPhrase'] + '.'
    Weather.objects.create(date_of=date_of, t_min=t_min, t_max=t_max, pressure=pressure,
                           description=description,
                           location_key_id=default_location_key)  # сохраняем информацию о погоде в БД
    return render(request, 'weather/get_info_about_location.html',
                  {'location_name': default_location_name,
                   'date_of': date_of,
                   't_min': t_min,
                   't_max': t_max,
                   'pressure': pressure,
                   'description': description})


def show_info_about_default_location(request):
    setting = Settings.objects.get()  # получение настроек приложения из БД
    default_location_key = setting.default_location_key_id
    location = Location.objects.filter(
        location_key__exact=default_location_key)  # получение данных из БД о названии локации по умолчанию
    default_location_name = str(location[0])
    all_info_about_default = Weather.objects.filter(
        location_key__exact=default_location_key)  # получение данных из БД о погоде в локации по умолчанию
    return render(request, 'weather/show_info_about_default_location.html',
                  {'default_location_name': default_location_name,
                   'all_info_about_default': all_info_about_default})


def search_location_for_get_weather(request):
    search_this = request.POST['location']  # получаем данные из формы на странице index.html
    search_this = search_this.strip()  # удаляем пробелы в начале и конце строки
    search_res = search_location_by_name(search_this)  # поиск локации
    # обработка, если локация не найдена
    if search_res == '':
        return render(request, 'weather/index.html', {
            'error_message': "Такая локация не найдена"})
    # проверяем, чтобы строка не содержала латиницы
    elif search_res == 'not a cyrillic':
        return render(request, 'weather/index.html', {
            'error_message': "Значение может содержать только кириллические буквы"})
    else:
        return render(request, 'weather/search_location_for_get_weather.html',
                      {'search_result': search_res})


def get_info_about_another_location(request):
    try:
        found_location_data = request.POST[
            'location']  # получаем данные из формы на странице search_location_for_get_weather.html
        list_of_fl_data = found_location_data.split(':')  # разделяем строку на ключ локации и ее название
        found_location_key = list_of_fl_data[0]
        found_location_name = list_of_fl_data[1]
        # проверяем, сохранена ли такая локация в БД, если нет - сохраняем
        saved_location = Location.objects.filter(location_key__exact=found_location_key)
        if not saved_location:
            Location.objects.create(location_key=found_location_key, location_name=found_location_name)
        # запрос информации о погоде с сайта accuweather.com
        url_forecast = 'http://dataservice.accuweather.com/forecasts/v1/daily/1day/'
        url_current_conditions = 'http://dataservice.accuweather.com/currentconditions/v1/'
        answer_for = send_request_about_weather_in_location(url_forecast, found_location_key)
        answer_cc = send_request_about_weather_in_location(url_current_conditions, found_location_key)
        d1 = datetime.datetime.strptime(answer_for['DailyForecasts'][0]['Date'],
                                        "%Y-%m-%dT%H:%M:%S%z")  # переводим дату в удобный формат
        date_of = d1.strftime('%d.%m.%Y')
        t_min = str(answer_for['DailyForecasts'][0]['Temperature']['Minimum']['Value']) + ' °' + \
                answer_for['DailyForecasts'][0]['Temperature']['Minimum']['Unit']
        t_max = str(answer_for['DailyForecasts'][0]['Temperature']['Maximum']['Value']) + ' °' + \
                answer_for['DailyForecasts'][0]['Temperature']['Maximum']['Unit']
        pressure = str(round(answer_cc[0]['Pressure']['Metric']['Value'] * 0.75006375541921)) + ' мм рт.ст. ' + \
                   answer_cc[0]['PressureTendency']['LocalizedText']
        description = 'Днём: ' + answer_for['DailyForecasts'][0]['Day']['LongPhrase'] + '. Ночью: ' + \
                      answer_for['DailyForecasts'][0]['Night']['LongPhrase'] + '.'
        Weather.objects.create(date_of=date_of, t_min=t_min, t_max=t_max, pressure=pressure,
                               description=description,
                               location_key_id=found_location_key)  # сохраняем информацию о погоде в БД
        return render(request, 'weather/get_info_about_location.html',
                      {'location_name': found_location_name,
                       'date_of': date_of,
                       't_min': t_min,
                       't_max': t_max,
                       'pressure': pressure,
                       'description': description})
    except (KeyError, MultiValueDictKeyError):
        return render(request, 'weather/search_location_for_get_weather.html',
                      {'error_message': 'Ошибка: значение не было выбрано'})


def show_all_saved_info_about_locations(request):
    all_saved_info = Weather.objects.all().order_by(
        'date_of')  # получаем информацию о погоде из БД с сортировкой по дате
    all_locations_names = Location.objects.all()  # получаем названия всех локаций, сохраненных в БД
    return render(request, 'weather/show_all_saved_info.html',
                  {'all_saved_info': all_saved_info,
                   'all_locations_names': all_locations_names})


def go_to_settings(request):
    return render(request, 'weather/go_to_settings.html')  # переходим к странице настроек


def search_location_for_change_settings(request):
    search_this = request.POST['location']  # получаем данные из формы на странице go_to_settings.html
    search_this = search_this.strip()  # удаляем пробелы в начале и конце строки
    search_res = search_location_by_name(search_this)  # поиск локации
    # обработка, если локация не найдена
    if search_res == '':
        return render(request, 'weather/go_to_settings.html', {
            'error_message_location': "Такая локация не найдена"})
    # проверяем, чтобы строка не содержала латиницы
    elif search_res == 'not a cyrillic':
        return render(request, 'weather/go_to_settings.html', {
            'error_message_location': "Значение может содержать только кириллические буквы"})
    else:
        return render(request, 'weather/search_location_for_change_settings.html',
                      {'search_result': search_res})


def change_api_key_in_settings(request):
    setting = Settings.objects.get()  # получение настроек приложения из БД
    current_default_location_key = setting.default_location_key_id
    new_api_key = request.POST['api_key']  # получаем данные из формы на странице go_to_settings.html
    new_api_key = new_api_key.strip()
    error_message_api = check_new_api_key_on_errors(new_api_key)  # проверяем введенное значение
    if error_message_api != '':
        return render(request, 'weather/go_to_settings.html', {
            'error_message_api': error_message_api})
    else:
        Settings.objects.all().delete()  # удаляем предыдущие значения настроек из БД
        Settings.objects.create(API_key=new_api_key,
                                default_location_key_id=current_default_location_key)  # сохраняем новые значения настроек в БД
        return render(request, 'weather/change_settings_result.html')


def change_default_location_in_settings(request):
    setting = Settings.objects.get()  # получение настроек приложения из БД
    current_api_key = setting.API_key
    try:
        found_location_data = request.POST[
            'change_location']  # получаем данные из формы на странице search_location_for_change_settings.html
        list_of_fl_data = found_location_data.split(':')  # разделяем строку на ключ локации и ее название
        new_default_location_key = list_of_fl_data[0]
        # проверяем, сохранена ли такая локация в БД, если нет - сохраняем
        saved_location = Location.objects.filter(location_key__exact=new_default_location_key)
        if not saved_location:
            Location.objects.create(location_key=new_default_location_key, location_name=list_of_fl_data[1])
        Settings.objects.all().delete()  # удаляем предыдущие значения настроек из БД
        Settings.objects.create(API_key=current_api_key,
                                default_location_key_id=new_default_location_key)  # сохраняем новые значения настроек в БД
        return render(request, 'weather/change_settings_result.html')
    except (KeyError, MultiValueDictKeyError):
        return render(request, 'weather/search_location_for_change_settings.html',
                      {'error_message': 'Ошибка: значение не было выбрано'})


def show_info_about_favorite_locations(request):
    favorite_locations_keys = FavoriteLocation.objects.all()
    info_about_favorite_locations = []
    for value in favorite_locations_keys:
        location_key = value.favorite_location_key
        row = {}
        # запрос информации о погоде с сайта accuweather.com
        url_forecast = 'http://dataservice.accuweather.com/forecasts/v1/daily/1day/'
        url_current_conditions = 'http://dataservice.accuweather.com/currentconditions/v1/'
        answer_for = send_request_about_weather_in_location(url_forecast, location_key)
        answer_cc = send_request_about_weather_in_location(url_current_conditions, location_key)
        d1 = datetime.datetime.strptime(answer_for['DailyForecasts'][0]['Date'],
                                        "%Y-%m-%dT%H:%M:%S%z")  # переводим дату в удобный формат
        date_of = d1.strftime('%d.%m.%Y')
        location_name = list(
            Location.objects.filter(location_key__exact=location_key))  # получаем название локации из БД
        row['location_name'] = location_name[0]
        t_min = str(answer_for['DailyForecasts'][0]['Temperature']['Minimum']['Value']) + ' °' + \
                answer_for['DailyForecasts'][0]['Temperature']['Minimum']['Unit']
        row['t_min'] = t_min
        t_max = str(answer_for['DailyForecasts'][0]['Temperature']['Maximum']['Value']) + ' °' + \
                answer_for['DailyForecasts'][0]['Temperature']['Maximum']['Unit']
        row['t_max'] = t_max
        pressure = str(round(answer_cc[0]['Pressure']['Metric']['Value'] * 0.75006375541921)) + ' мм рт.ст. ' + \
                   answer_cc[0]['PressureTendency']['LocalizedText']
        row['pressure'] = pressure
        description = 'Днём: ' + answer_for['DailyForecasts'][0]['Day']['LongPhrase'] + '. Ночью: ' + \
                      answer_for['DailyForecasts'][0]['Night']['LongPhrase'] + '.'
        row['description'] = description
        Weather.objects.create(date_of=date_of, t_min=t_min, t_max=t_max, pressure=pressure,
                               description=description,
                               location_key_id=location_key)  # сохраняем информацию о погоде в БД
        info_about_favorite_locations.append(row)
    return render(request, 'weather/show_info_about_favorite_locations.html',
                  {'info_about_favorite_locations': info_about_favorite_locations})


def change_cron_settings(request):
    favorite_locations_keys = FavoriteLocation.objects.all()  # получаем информацию о локациях для планировщика, сохраненных в БД
    favorite_locations = {}
    for key in favorite_locations_keys:
        location = Location.objects.filter(location_key__exact=key.favorite_location_key)  # получаем названия локаций
        favorite_locations[key.favorite_location_key] = location[0]
    return render(request, 'weather/settings_for_cron.html',
                  {'favorite_locations': favorite_locations})


def delete_favorite_location_from_cron_settings(request):
    del_location_key = request.POST['favlocation']
    FavoriteLocation.objects.filter(favorite_location_key__exact=del_location_key).delete()
    favorite_locations_keys = FavoriteLocation.objects.all()  # получаем информацию о локациях для планировщика, сохраненных в БД
    favorite_locations = {}
    for key in favorite_locations_keys:
        location = Location.objects.filter(location_key__exact=key.favorite_location_key)  # получаем названия локаций
        favorite_locations[key.favorite_location_key] = location[0]
    return render(request, 'weather/settings_for_cron.html',
                  {'favorite_locations': favorite_locations})


def search_location_for_add_favorite_in_cron_settings(request):
    search_this = request.POST['location']  # получаем данные из формы на странице settings_for_cron.html
    search_this = search_this.strip()  # удаляем пробелы в начале и конце строки
    search_res = search_location_by_name(search_this)  # поиск локации
    # обработка, если локация не найдена
    if search_res == '':
        return render(request, 'weather/go_to_settings.html', {
            'error_message_location': "Такая локация не найдена"})
    # проверяем, чтобы строка не содержала латиницы
    elif search_res == 'not a cyrillic':
        return render(request, 'weather/go_to_settings.html', {
            'error_message_location': "Значение может содержать только кириллические буквы"})
    else:
        return render(request, 'weather/search_location_for_add_in_cron_settings.html',
                      {'search_result': search_res})


def add_favorite_location_in_cron_settings(request):
    try:
        found_location_data = request.POST[
            'add_location']  # получаем данные из формы на странице search_location_for_add_in_cron_settings.html
        list_of_fl_data = found_location_data.split(':')  # разделяем строку на ключ локации и ее название
        # проверяем, сохранена ли такая локация в БД, если нет - сохраняем
        saved_location = Location.objects.filter(location_key__exact=list_of_fl_data[0])
        if not saved_location:
            Location.objects.create(location_key=list_of_fl_data[0], location_name=list_of_fl_data[1])
        FavoriteLocation.objects.create(favorite_location_key=list_of_fl_data[0])  # сохраняем новое значение в БД
        favorite_locations_keys = FavoriteLocation.objects.all()  # получаем информацию о локациях для планировщика, сохраненных в БД
        favorite_locations = {}
        for key in favorite_locations_keys:
            location = Location.objects.filter(
                location_key__exact=key.favorite_location_key)  # получаем названия локаций
            favorite_locations[key.favorite_location_key] = location[0]
        return render(request, 'weather/settings_for_cron.html',
                      {'favorite_locations': favorite_locations})
    except (KeyError, MultiValueDictKeyError):
        return render(request, 'weather/search_location_for_add_in_cron_settings.html',
                      {'error_message': 'Ошибка: значение не было выбрано'})

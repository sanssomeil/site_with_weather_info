import re

import requests

from weather.models import Settings


def check_query_on_errors(query: str) -> str:
    """ проверяем, чтобы введенная для поиска строка не содержала латинских букв,
    т.к. отправляем запрос с параметром 'ru'"""
    if len(re.findall(r'[^а-яА-Я\-]', query)) > 0:
        search_result = 'not a cyrillic'
        return search_result
    else:
        return ''


def search_location_by_name(query):
    return search_location_by_name_with_query_set(Settings.objects, query)


def search_location_by_name_with_query_set(querySet, query: str):
    """Выполняет поиск локации"""
    search_result = check_query_on_errors(query)
    if search_result == '':
        try:
            url_city = 'http://dataservice.accuweather.com/locations/v1/cities/autocomplete'
            settings_app = querySet.get()  # получение настроек приложения из БД
            api_key = settings_app.API_key
            payload = {'apikey': api_key,
                       'q': query,
                       'language': 'ru'}  # параметры запроса
            response = requests.get(url_city, params=payload)  # запрос на поиск локации
            answer = response.json()
            search_result = {}
            i = 0
            # запись нужных данных из ответа сервера
            for key in answer:
                field_of = ''
                field_of += str(answer[i]['LocalizedName']) + ', '
                field_of += str(answer[i]['AdministrativeArea']['LocalizedName']) + ', '
                field_of += str(answer[i]['Country']['LocalizedName'])
                search_result[str(answer[i]['Key'])] = field_of
                i += 1
            if len(search_result) != 0:  # если есть результаты поиска, возвращаем их
                return search_result
            else:
                return ''  # если ничего не найдено, возвращаем пустую строку

        except KeyError:
            print('Кажется, нужно сменить API key.')

        except Exception as err:
            print('Что-то пошло не так с поиском локации - ', str(err))

    else:
        return search_result

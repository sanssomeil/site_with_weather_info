import re
from unittest.mock import Mock
from django.test import TestCase

from weather.check_new_api_key import check_new_api_key_on_errors
from weather.models import Weather, Location, Settings

from weather.search_location import search_location_by_name_with_query_set, search_location_by_name


class ModelsTestCase(TestCase):

    def test_check_on_latin_in_query(self):
        """
        search_location_by_name возвращает 'not a cyrillic', если во введенном значении содержатся латинские буквы
        """
        location = 'abcdefg'
        result = search_location_by_name(location)
        self.assertEqual(result, 'not a cyrillic')

    def test_check_on_cyrillic_in_query(self):
        """
        search_location_by_name_with_query_set не возвращает 'not a cyrillic', если во введенном значении содержатся только кириллица и дефисы
        """
        location = 'улан-удэ'
        settings = Mock(spec=Settings.objects)
        settings.get.return_value = Settings('00000001', 'hJT7AjCQaou8Wdpvr5MJuH9w8MvO9HjD')
        self.assertNotEqual(search_location_by_name_with_query_set(settings, location), 'not a cyrillic')

    def test_check_on_unfound_value_in_query(self):
        """
        search_location_by_name_with_query_set возвращает пустую строку,
        если по введенному значению не найдена ни одна локация
        """
        location = 'йцукен'
        settings = Mock(spec=Settings.objects)
        settings.get.return_value = Settings('00000001', 'hJT7AjCQaou8Wdpvr5MJuH9w8MvO9HjD')
        self.assertEqual(search_location_by_name_with_query_set(settings, location), '')

    def test_check_API_key_on_empty_value(self):
        """
        check_new_api_key_on_errors возвращает ошибку 'Значение не может быть пустым',
        если значение не было введено перед нажатием кнопки на форме
        """
        new_api_key = ''
        self.assertEqual(check_new_api_key_on_errors(new_api_key), 'Значение не может быть пустым')

    def test_check_API_key_on_value_length(self):
        """
        check_new_api_key_on_errors проверяет введенное значение на длину и возвращает ошибку
        'API key должен содержать 32 символа', если длина значения не соответствует 32
        """
        new_api_key = '1234567890'
        self.assertEqual(check_new_api_key_on_errors(new_api_key), 'API key должен содержать 32 символа')

    def test_check_API_key_on_rignt_value(self):
        """
        check_new_api_key_on_errors проверяет введенное значение возвращает пустую строку, если ошибок не обраружено
        """
        new_api_key = '1Nlw4e8oFD97yUwKxMJFCqMikO1NuJQB'  # строка сгенерирована автоматически и не является реальным значением API key
        self.assertEqual(check_new_api_key_on_errors(new_api_key), '')

from django.contrib import admin

from django.contrib import admin

from .models import Weather, Location, Settings

admin.site.register(Weather)
admin.site.register(Location)
admin.site.register(Settings)
from django.db import models


class Location(models.Model):
    location_key = models.CharField(max_length=8, primary_key=True)
    location_name = models.CharField(max_length=256)

    def __str__(self):
        return self.location_name


class Weather(models.Model):
    date_of = models.CharField(max_length=10)
    t_min = models.CharField(max_length=10)
    t_max = models.CharField(max_length=10)
    pressure = models.CharField(max_length=30)
    description = models.CharField(max_length=256)
    location_key = models.ForeignKey(Location, on_delete=models.CASCADE)

    def save_info_in_database(self):
        self.save()


class Settings(models.Model):
    API_key = models.CharField(max_length=32)
    default_location_key = models.ForeignKey(Location, on_delete=models.CASCADE, null=True)


class FavoriteLocation(models.Model):
    favorite_location_key = models.CharField(max_length=8)

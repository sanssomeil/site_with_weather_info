from django_cron import CronJobBase, Schedule
import webbrowser


class MyCronJob(CronJobBase):
    RUN_AT_TIMES = ['08:00', '16:00', '20:46']
    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = 'weather.my_cron_job'  # a unique code

    def do(self):
        webbrowser.open('http://127.0.0.1:8000/weather/show-favorite-locations/')
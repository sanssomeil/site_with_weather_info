import requests
from weather.models import Settings


def send_request_about_weather_in_location(url: str, locationKey: str) -> 'json':
    """Запрашивает данные о погоде с сервера"""
    try:
        settings_app = Settings.objects.get() # получение настроек приложения из БД
        api_key = settings_app.API_key
        payload = {'apikey': api_key,
                   'language': 'ru',
                   'details': 'true',
                   'metric': 'true'}  # параметры запроса
        urlreq = url + locationKey
        response = requests.get(urlreq, params=payload)  # запрос информации о погоде
        answer = response.json()
        return answer

    except KeyError:
        print('Кажется, нужно сменить API key.')

    except Exception as err:
        print('Что-то пошло не так с запросом - ', str(err))
